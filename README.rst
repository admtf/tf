========
Jogo UNO
========

:Author: GoHorse

| 

Desenvolvimento
===============

Ferramentas Utilizadas
----------------------

A fim de agilizar a iteração do desenvolvimento do jogo, escolhemos usar
a mesma ferramenta usada em projetos Android chamada Gradle. Ela possui
algumas funcionalidades que facilitaram o desenvolvimento de software em
equipe.

#. Roda em qualquer OS, mesmo sem a ferramenta instalada.
#. Padroniza os comandos e tarefas nas comuns nas etapas de
   desenvolvimento e distribuição
#. Baixa e controla as dependências do programa

| 

Dependências
------------

-  launch4j
-  gradle.plugin.edu.sc.seis.gradle:launch4j:2.3.0"
-  gradle.plugin.com.palantir:jacoco-coverage:0.4.0"
-  junit:junit:4.12
-  org.junit.contrib.junit-theories:4.12
-  com.github.stefanbirkner:system-rules:1.16.1

| 

Testes
======

| 

Testes Unitários
----------------

Os teste de integração são basicamente a classe Carta, e Lista.

| 
| |image0|

| 
| |image1|

| 

Testes de Integração
--------------------

Os teste de integração são basicamente a classe Setup e Uno.

| 
| |image2|

| 
| |image3|

Artefatos Gerados
=================

Segue tabela informando os artefatos gerando pelo Gradle.

+----------------------------+------------------------------+-------------------+
| Artefacto                  | Localização no repositório   | Gradle Plugin     |
+============================+==============================+===================+
| Código Fonte               | ./src/                       | ---               |
+----------------------------+------------------------------+-------------------+
| JavaDoc Website            | ./build/docs/javadoc         | Native Gradle     |
+----------------------------+------------------------------+-------------------+
| Jar                        | ./build/libs/tf.jar          | Native Gradle     |
+----------------------------+------------------------------+-------------------+
| Relatórios Test Coverage   | ./build/reports/jacoco       | Jacoco Plugin     |
+----------------------------+------------------------------+-------------------+
| Window .exe                | ./build/launch4j             | Gradle-launch4j   |
+----------------------------+------------------------------+-------------------+

| 

Javadoc
-------

|image4|

.. |image0| image:: static/carta.png
.. |image1| image:: static/lista.png
.. |image2| image:: static/setup.png
.. |image3| image:: static/uno.png
.. |image4| image:: static/javadoc.png
