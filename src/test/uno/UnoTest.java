package uno;

import static org.junit.Assert.assertEquals;

import org.junit.Rule;

import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import uno.Uno;

public class UnoTest {
    @Rule
    public final SystemOutRule  log = new SystemOutRule().enableLog();

	private Uno newGame;

	@Test
	public void numeroDeJogadores(){
		newGame = new Uno();
		newGame.inicializacaoGeral(2);
		assertEquals(newGame.jogadores.length,2);
	}
	@Test
	public void escolha(){
		newGame = new Uno();
		newGame.inicializacaoGeral(2);
		assertEquals(newGame.escolha,-2);
	}

}
