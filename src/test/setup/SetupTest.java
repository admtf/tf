package setup;

import java.io.File;
import org.junit.Rule;
import org.junit.Before;
import org.junit.After;

import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import uno.Uno;

public class SetupTest {
    @Rule
    public final SystemOutRule  log = new SystemOutRule().enableLog();

	private Uno newGame;

	@Before
	public void init(){
		newGame = new Uno();
		newGame.inicializacaoGeral(2);
	}
	@After
	public void after(){
		File f = null;
		try {
			f = new File("saveLoadBaralho.txt");
			f.delete();
			f = new File("saveLoadMesa.txt");
			f.delete();
			f = new File("saveLoadJogadores.txt");
			f.delete();
			f = new File("saveLoadConfig.txt");
			f.delete();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void saveandload() {
		Setup.save(newGame);
		Setup.load(newGame);
	}
}
