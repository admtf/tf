package lista;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import carta.Carta;

public class ListaTest {
    @Rule
    public final SystemOutRule  log = new SystemOutRule().enableLog();
    
    
	private Lista lista = new Lista();

	@Test
	public void size() {
		assertEquals("Brand new List size is zero", lista.size(), 0, 0);
	}

	@Test
	public void isEmpty() {
		assertTrue(lista.isEmpty());
	}

	@Test
	public void indexOf() {
		Carta essaCarta = new Carta("0", "vermelho", "0");
		lista.add(essaCarta);
		assertTrue(lista.indexOf(essaCarta) == 0);
		lista.remove(essaCarta);
	}
	@Test
	public void contains() {
		Carta essaCarta = new Carta("0", "vermelho", "0");
		lista.add(essaCarta);
		assertTrue(lista.contains(essaCarta));
		lista.remove(essaCarta);
	}
	@Test
	public void get() {
		Carta essaCarta = new Carta("0", "vermelho", "0");
		lista.add(essaCarta);
		assertTrue(lista.get(0) == essaCarta);
		lista.remove(essaCarta);
	}
	@Test
	public void indiceValido() {
		Carta essaCarta = new Carta("0", "vermelho", "0");
		lista.add(essaCarta);
		assertTrue(lista.indiceValido(0));
		lista.remove(essaCarta);
	}
	@Test
	public void printaUltima() {
		Carta essaCarta = new Carta("0", "vermelho", "0");
		lista.add(essaCarta);
		lista.printaUltima();
        assertEquals("0 - vermelho\n", log.getLog());
		lista.remove(essaCarta);
	}
	@Test
	public void remove() {
		Carta essaCarta = new Carta("0", "vermelho", "0");
		lista.add(essaCarta);
		lista.remove(essaCarta);
		assertEquals(lista.size(),0);
	}

	@Test
	public void removeVI() {
		Carta essaCarta = new Carta("0", "vermelho", "0");
		lista.add(essaCarta);
		lista.remove(0);
		assertEquals(lista.size(),0);
	}

	@Test
	public void adder() {
		Carta essaCarta = new Carta("0", "vermelho", "0");
		lista.add(3,essaCarta);
		assertEquals(lista.size(),1);
		// lista.add(3000,essaCarta);
	}

	@Test
	public void embaralhar() {
		Carta essaCarta = new Carta("0", "azul", "0");
		for(int i = 0; i < 10; i++){
			lista.add(new Carta(String.valueOf(i), "vermelho", "0"));
		}
		lista.embaralhar();
		assertNotEquals(lista.indexOf(essaCarta),0);
	}
	@Test
	public void removeII() {
		Carta essaCarta = new Carta("0", "vermelho", "0");
		lista.add(essaCarta);
		lista.remove(0);
		assertEquals(lista.size(),0);
	}

	@Test
	public void removeIII() {
		Carta essaCarta = new Carta("0", "vermelho", "0");
		lista.add(essaCarta);
		lista.remove();
		assertEquals(lista.size(),0);
	}
	@Test
	public void TtoString() {
		Carta essaCarta = new Carta("0", "vermelho", "0");
		lista.add(essaCarta);
		System.out.println(lista.toString());
		assertEquals("[ 0 vermelho ]\n",log.getLog());
	}

	@Test
	public void crescerLista() {
		for(int i = 0; i < 10000; i++){
			lista.add(new Carta(String.valueOf(i), "vermelho", "0"));
		}
		assertEquals( lista.size() , 10000);
	}
	@Test
	public void getLast() {
		Carta essaCarta = new Carta("0", "vermelho", "0");
		lista.add(essaCarta);
		assertEquals( lista.getLast(), essaCarta);
	}
	@Test
	public void printaTexto() {
		Carta essaCarta = new Carta("0", "vermelho", "0");
		lista.add(essaCarta);
		lista.printaTexto();
		assertEquals( "0. 0 - vermelho\n",  log.getLog());
	}
	@Test
	public void set() {
		Carta essaCarta = new Carta("0", "vermelho", "0");
		lista.add(essaCarta);
		try{
			lista.set(0, essaCarta);
		}catch(Exception e){
			System.out.println(e);
		};
		lista.printaTexto();
		assertEquals( "0. 0 - vermelho\n",  log.getLog());
	}

	@Test
	public void removeIV() {
		assertEquals(lista.remove(), null);
	}
	@Test
	public void removeV() {
		assertEquals(lista.remove(0), null);
	}

	@Test
	public void Adding() {
		Carta essaCarta = new Carta("0", "vermelho", "0");
		lista.add(0, essaCarta);
		assertEquals(lista.indexOf(essaCarta),0);
	}

}
