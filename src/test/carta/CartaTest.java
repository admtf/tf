package carta;

import static org.junit.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

public class CartaTest {
    @Rule
    public final SystemOutRule  log = new SystemOutRule().enableLog();

	private Carta carta = new Carta("0", "azul", "0");

	@Test
	public void getEfeito() {
		assertEquals(carta.getEfeito(), "0");
	}

	@Test
	public void setCor() {
		carta.setCor("azul");
		assertEquals(carta.getCor(),"azul");
	}
}
