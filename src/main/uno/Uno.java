package uno;

import java.util.Scanner;
import carta.Carta;
import lista.Lista;
import setup.Setup;

public class Uno {
	public  Lista baralho;
	public  Lista mesa;
	public  Lista[] jogadores;
	public  int qtdJogadores = 0;
	public  int vezDoJogador = 0;
	public  int definidorDeVez = 1;
	public  Boolean passaVez = false;
	public  Boolean terminou = false;
	public  int escolha = -2;
	public  Carta[] deck = { 
		new Carta("0", "vermelho", "0"), new Carta("0", "vermelho", "0"),
		new Carta("1", "vermelho", "0"), new Carta("1", "vermelho", "0"), new Carta("2", "vermelho", "0"),
		new Carta("2", "vermelho", "0"), new Carta("3", "vermelho", "0"), new Carta("3", "vermelho", "0"),
		new Carta("4", "vermelho", "0"), new Carta("4", "vermelho", "0"), new Carta("5", "vermelho", "0"),
		new Carta("5", "vermelho", "0"), new Carta("6", "vermelho", "0"), new Carta("6", "vermelho", "0"),
		new Carta("7", "vermelho", "0"), new Carta("7", "vermelho", "0"), new Carta("8", "vermelho", "0"),
		new Carta("8", "vermelho", "0"), new Carta("9", "vermelho", "0"), new Carta("9", "vermelho", "0"),
		new Carta("0", "verde", "0"), new Carta("0", "verde", "0"), new Carta("1", "verde", "0"),
		new Carta("1", "verde", "0"), new Carta("2", "verde", "0"), new Carta("2", "verde", "0"),
		new Carta("3", "verde", "0"), new Carta("3", "verde", "0"), new Carta("4", "verde", "0"),
		new Carta("4", "verde", "0"), new Carta("5", "verde", "0"), new Carta("5", "verde", "0"),
		new Carta("6", "verde", "0"), new Carta("6", "verde", "0"), new Carta("7", "verde", "0"),
		new Carta("7", "verde", "0"), new Carta("8", "verde", "0"), new Carta("8", "verde", "0"),
		new Carta("9", "verde", "0"), new Carta("9", "verde", "0"), new Carta("0", "amarelo", "0"),
		new Carta("0", "amarelo", "0"), new Carta("1", "amarelo", "0"), new Carta("1", "amarelo", "0"),
		new Carta("2", "amarelo", "0"), new Carta("2", "amarelo", "0"), new Carta("3", "amarelo", "0"),
		new Carta("3", "amarelo", "0"), new Carta("4", "amarelo", "0"), new Carta("4", "amarelo", "0"),
		new Carta("5", "amarelo", "0"), new Carta("5", "amarelo", "0"), new Carta("6", "amarelo", "0"),
		new Carta("6", "amarelo", "0"), new Carta("7", "amarelo", "0"), new Carta("7", "amarelo", "0"),
		new Carta("8", "amarelo", "0"), new Carta("8", "amarelo", "0"), new Carta("9", "amarelo", "0"),
		new Carta("9", "amarelo", "0"), new Carta("0", "azul", "0"), new Carta("0", "azul", "0"),
		new Carta("1", "azul", "0"), new Carta("1", "azul", "0"), new Carta("2", "azul", "0"),
		new Carta("2", "azul", "0"), new Carta("3", "azul", "0"), new Carta("3", "azul", "0"),
		new Carta("4", "azul", "0"), new Carta("4", "azul", "0"), new Carta("5", "azul", "0"),
		new Carta("5", "azul", "0"), new Carta("6", "azul", "0"), new Carta("6", "azul", "0"),
		new Carta("7", "azul", "0"), new Carta("7", "azul", "0"), new Carta("8", "azul", "0"),
		new Carta("8", "azul", "0"), new Carta("9", "azul", "0"), new Carta("9", "azul", "0") };

	public Uno() {}

	public  void geraBaralho() {
		baralho = new Lista();
		for (int i = 0; i < deck.length; i++) {
			baralho.add(deck[i]);
		}
		baralho.embaralhar();
	}

	// Cada mao compra a quantidade inicial de cartas do jogo
	public  void iniciaMaos() {
		Carta teste;
		int qtdCartaInicial = 7;
		for (int i = 0; i < qtdJogadores; i++) {
			for (int j = 0; j < qtdCartaInicial; j++) {
				teste = baralho.remove();
				jogadores[i].add(teste);
			}
		}
	}

	public  void compraCarta() {
		Carta aux = baralho.remove();
		jogadores[vezDoJogador].add(aux);
		System.out.println("---------------------------------");
		System.out.print("Você comprou a carta: ");
		jogadores[vezDoJogador].printaUltima();
		System.out.println("---------------------------------");
		for (int i = 0; i < 7; i++) {
			System.out.println();
		}
	}

	public  void printaMaos() {
		for (int i = 0; i < qtdJogadores; i++) {
			System.out.println("Jogador " + (i + 1) + ": ");
			jogadores[i].printaTexto();
		}
	}

	public  void geraProximoJogador() {
		if (vezDoJogador == qtdJogadores - 1 && definidorDeVez == 1) {
			vezDoJogador = 0;
		} else if (vezDoJogador == 0 && definidorDeVez == -1) {
			vezDoJogador = qtdJogadores - 1;
		} else {
			vezDoJogador += definidorDeVez;
		}
	}

	public void Run() {
		int resposta = 0;

		while (!terminou) {
			while (!passaVez) {

				passaVez = true;
				System.out.println("---------------------------------");
				System.out.println("              Mesa");
				System.out.print("          ");
				mesa.printaUltima();
				System.out.println("---------------------------------");
				System.out.println();
				System.out.println("Vez do Jogador " + (vezDoJogador + 1) + ": ");
				System.out.print("1. Mostrar Mão \n");
				System.out.print("2. Jogar Carta \n");
				System.out.print("3. Comprar Carta \n");
				System.out.print("4. Sair do jogo\n");
				System.out.print("5. Salvar jogo \n");
				System.out.print("6. Carregar jogo \n");
				System.out.println("Opção: ");

				Scanner entrada = new Scanner(System.in);
				resposta = entrada.nextInt();
				switch (resposta) {
				case 1:
					limpaTela();
					mostraMao();
					System.out.println();
					passaVez = false;
					break;
				case 2:
					limpaTela();
					Boolean trocaRodada = false;
					// Mostra a mão
					mostraMao();
					System.out.println("\nQual carta deseja jogar?");
					while ((escolha < 0 || escolha > jogadores[vezDoJogador].size() - 1) && escolha != -1
						   && trocaRodada == false) {
						escolha = entrada.nextInt();
						if (escolha < 0 || escolha > jogadores[vezDoJogador].size() - 1) {
							limpaTela();
							System.out.println("Índice inválido.");
							System.out.println("Caso queira voltar atras, digite -1");
							mostraMao();
							System.out.println("\nQual carta deseja jogar?");
						}
						trocaRodada = verificaJogadaValida();
					}
					limpaTela();
					escolha = -2; // reseta a escolha para padrão
					passaVez = false; // reseta opcao do menu
					break;
				case 3:
					limpaTela();
					compraCarta();
					delay(1500);
					limpaTela();
					geraProximoJogador();
					passaVez = false;
					break;
				case 4:
					passaVez = true;
					terminou = true;
					break;
				case 5:
					limpaTela();
					Setup.save(this);
					System.out.println();
					passaVez = false;
					break;
				case 6:
					limpaTela();
					Setup.load(this);
					System.out.println();
					passaVez = false;
					break;
				default:
					limpaTela();
					passaVez = false;
				}
			}
		}
	}

	public void adicoinarJogadores(){
		jogadores = new Lista[qtdJogadores];
		for (int i = 0; i < qtdJogadores; i++) {
			Lista jogador = new Lista();
			jogadores[i] = jogador;
		}
	}
	public void inicializacaoGeral(int players) {
		qtdJogadores = players;
		geraBaralho();
		adicoinarJogadores();
		// inicializando jogadores
		iniciaMaos();
		virarCarta();

	}

	public void virarCarta() {
		mesa = new Lista();
		Carta teste = baralho.remove();
		mesa.add(teste);
		limpaTela();

	}

	public  void informaSeValeu(String mensagem) {
		// Informa na tela se valeu ou não
		limpaTela();
		System.out.println("---------------------------------");
		System.out.println(mensagem);
		System.out.println("---------------------------------");
		for (int i = 0; i < 7; i++) {
			System.out.println();
		}
		delay(1000);
		limpaTela();
	}

	public  Boolean verificaJogadaValida() {
		if (escolha > -1 && escolha < jogadores[vezDoJogador].size()) {
			if (((jogadores[vezDoJogador].get(escolha).getNome().equals(mesa.getLast().getNome()))
				 || (jogadores[vezDoJogador].get(escolha).getCor().equals(mesa.getLast().getCor())))
				|| jogadores[vezDoJogador].get(escolha).getNome().equals("Cur")
				|| jogadores[vezDoJogador].get(escolha).getNome().equals("+4")) {
				Carta aux = jogadores[vezDoJogador].remove(escolha);
				mesa.add(aux);
				// Informa na tela se valeu ou não
				informaSeValeu("Carta jogada.");
				// Verifica se o jogador ficou com 0 cartas
				verificaSeGanhou();
				// Executa os efeitos das cartas
				efeitosEspeciais();
				return true;
			} else {
				// Informa na tela se valeu ou não
				informaSeValeu("Carta inválida.");
				return false;
			}
		}
		return false;
	}

	public  void efeitosEspeciais() {
		String aux = mesa.getLast().getEfeito();
		int efeito = Integer.parseInt(aux);
		Scanner entrada = new Scanner(System.in);
		Carta auxCarta;
		int cor = -1;
		switch (efeito) {
		case 0:
			// Simplesmente passa a rodadas
			geraProximoJogador();
			break;
		case 1:
			// Adiciona 2 cartas para o jogador seguinte
			geraProximoJogador();
			auxCarta = baralho.remove();
			jogadores[vezDoJogador].add(auxCarta);
			auxCarta = baralho.remove();
			jogadores[vezDoJogador].add(auxCarta);
			System.out.println("Jogador: " + (vezDoJogador + 1) + " recebeu +2 e perdeu a vez.");
			geraProximoJogador();
			break;
		case 2:
			// Inverte o sentido do jogo
			if (this.definidorDeVez == 1) {
				definidorDeVez = -1;
			} else {
				definidorDeVez = 1;
			}
			System.out.println("Sentido do jogo foi invertido.");
			geraProximoJogador();
			break;
		case 3:
			// Bloqueia o próximo jogador
			geraProximoJogador();
			System.out.println("Jogador " + (vezDoJogador + 1) + " perdeu a vez");
			geraProximoJogador();
			break;
		case 4:
			// Efeito curinga, escolhe a cor da carta
			while (cor < 0 || cor > 3) {
				System.out.println("---------------------------------");
				System.out.println("Cores para o curinga");
				System.out.println("---------------------------------");
				System.out.println("0. Amarelo \n1. Azul \n2. Verde \n3. Vermelho\n");
				System.out.println("Qual a cor desejada?");
				cor = entrada.nextInt();
				switch (cor) {
				case 0:
					mesa.getLast().setCor("amarelo");
					break;
				case 1:
					mesa.getLast().setCor("azul");
					break;
				case 2:
					mesa.getLast().setCor("verde");
					break;
				case 3:
					mesa.getLast().setCor("vermelho");
					break;
				default:
					limpaTela();
				}
			}
			geraProximoJogador();
			System.out.println("Jogador: " + (vezDoJogador + 1) + " recebeu +4 e perdeu a vez.");
			limpaTela();
			break;
		case 5:
			// Efeito curinga, escolhe a cor da carta
			while (cor < 0 || cor > 3) {
				System.out.println("---------------------------------");
				System.out.println("Cores para o +4");
				System.out.println("---------------------------------");
				System.out.println("0. Amarelo \n1. Azul \n2. Verde \n3. Vermelho\n");
				System.out.println("Qual a cor desejada?");
				cor = entrada.nextInt();
				switch (cor) {
				case 0:
					mesa.getLast().setCor("amarelo");
					break;
				case 1:
					mesa.getLast().setCor("azul");
					break;
				case 2:
					mesa.getLast().setCor("verde");
					break;
				case 3:
					mesa.getLast().setCor("vermelho");
					break;
				default:
					limpaTela();
				}
			}
			limpaTela();
			geraProximoJogador();
			// Adiciona 4 cartas na mao do jogador
			for (int i = 0; i < 4; i++) {
				auxCarta = baralho.remove();
				jogadores[vezDoJogador].add(auxCarta);
			}
			System.out.println("Jogador: " + (vezDoJogador + 1) + " recebeu +4 e perdeu a vez.");
			geraProximoJogador();
			break;
		default:
			;
		}
	}

	public  void verificaSeGanhou() {
		if (jogadores[vezDoJogador].size() == 0) {
			System.out.println("---------------------------------");
			System.out.println("Jogador " + (vezDoJogador + 1) + " ganhou!!!");
			System.out.println("---------------------------------");
			System.out.println("\n\n\n\n\n");
			System.exit(0);
		}
	}

	public  void mostraMao() {
		System.out.println("---------------------------------");
		System.out.println("Mão do Jogador " + (vezDoJogador + 1) + ": ");
		System.out.println("---------------------------------");
		jogadores[vezDoJogador].printaTexto();
	}

	public  void delay(int tempo) {
		try {
			Thread.sleep(tempo);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public  void limpaTela() {
		for (int i = 0; i < 50; i++) {
			System.out.println();
		}
	}
}
