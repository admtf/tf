package app;

import java.util.Scanner;
import uno.Uno;

public class App {
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		Uno newGame = new Uno();
		int players = 133;
		System.out.println("Selecione a quantidade de jogadores: ");
		while (players < 1 || players > 10) {
			players = entrada.nextInt();
		}
		newGame.inicializacaoGeral(players);	
		newGame.Run();
	}
}
